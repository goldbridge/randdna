#include <iostream>
#include <string>
#include <random>

using namespace std;

string randDNA(int seed, string bases, int n)
{
    string randSeed = "";
    int min = 0;
    int max = bases.size() -1; // .size returns the size of the variable bases
    
    std::mt19937 eng2(seed); // this engine produces the same output everytime 
    
    std::uniform_int_distribution<int> unifrm(min, max); // returns numbers between min and max
    
    for(int i=0; i < n; i++) {
        
        int r = unifrm(eng2); // 
        
        randSeed = randSeed + bases[r];
    }
    return randSeed;

}
